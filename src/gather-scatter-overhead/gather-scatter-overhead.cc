// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <memory>
#include <thread>
#include <chrono>
#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/finiteelementmap/blockstructuredqkfem.hh>
#include <dune/pdelab/finiteelementmap/qkdg.hh>
#include <dune/pdelab/gridoperator/blockstructured.hh>
#include <dune/pdelab/backend/istl.hh>
#include "dune/ch-blockstructured/grid.hh"
#include "dune/ch-blockstructured/Q0Coefficient.hh"
#include "dune/ch-blockstructured/localoperator/gather-scatter-overhead/quadrature_loop_reordered.hh"


template<typename GFSU, typename GFSV, typename LOP,
    typename MB, typename DF, typename RF, typename JF,
    int k,
    typename CU=Dune::PDELab::EmptyTransformation,
    typename CV=Dune::PDELab::EmptyTransformation,
    std::enable_if_t<(k > 2), int> = 0
>
Dune::PDELab::BlockstructuredGridOperator<GFSU, GFSV, LOP, MB, DF, RF, JF, CU, CV> go_switch();
template<typename GFSU, typename GFSV, typename LOP,
    typename MB, typename DF, typename RF, typename JF,
    int k,
    typename CU=Dune::PDELab::EmptyTransformation,
    typename CV=Dune::PDELab::EmptyTransformation,
    std::enable_if_t<(k <= 2), int> = 0
>
Dune::PDELab::GridOperator<GFSU, GFSV, LOP, MB, DF, RF, JF, CU, CV> go_switch();


int main(int argc, char** argv)
{
  using Dune::PDELab::Backend::native;
  // Maybe initialize MPI
  Dune::FakeMPIHelper::instance(argc, argv);

  unsigned int N = DEFAULT_GRID_SIZE;
  if(argc > 1)
    N = std::stoi(argv[1]);
  auto grid_ptr = Dune::StructuredGridFactory<Grid>::createCubeGrid(Dune::FieldVector<double, 2>{0. ,0.},
                                                                    Dune::FieldVector<double, 2>{1., 1.},
                                                                    std::array<unsigned int, 2>{N, N});

  GV gv = grid_ptr->leafGridView();

  print_grid_type();

  using FEM = Dune::PDELab::BlockstructuredQkLocalFiniteElementMap<GV, double, double, 1, N_BLOCKS>;
  FEM fem(gv);

  using GFS = Dune::PDELab::GridFunctionSpace<GV, FEM>;
  GFS gfs(gv, fem);
  gfs.update();

  std::cout << "DOF " << gfs.size() << std::endl;

  const double bandwidth = 100 * 1e9;
  auto expected_iterations = [&] (const int n_streams) {
    const auto bytes_per_stream = gfs.size() * 8;
    const auto bytes = n_streams * bytes_per_stream * 20;
    return std::max(int(((bandwidth * 0.75) / bytes) * (double(quad_size(2, 2)) / quad_size(QUAD_ORDER, 2))), 1);
  };

  using FEM_Q0 = Dune::PDELab::QkDGLocalFiniteElementMap<double, double, N_BLOCKS - 1, 2>;
  FEM_Q0 fem_q0;

  using GFS_Q0 = Dune::PDELab::GridFunctionSpace<GV, FEM_Q0>;
  GFS_Q0 gfs_q0(gv, fem_q0);

  using V_Q0 = Dune::PDELab::Backend::Vector<GFS_Q0, double>;
  V_Q0 d(gfs_q0, 0.);

  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  MB mb(9);

  LOP lop(gv);

  using GO = decltype(go_switch<GFS, GFS, LOP, MB, double, double, double, N_BLOCKS>());
  GO go(gfs, gfs, lop, mb);

  using V = typename GO::Domain;
  V x(gfs, 0);
  V y(gfs, 0.);

  x = 1 + expected_iterations(2);

  std::cout << "IT " << expected_iterations(3) << std::endl;
#ifndef MEASURE_LOP
  auto start = std::chrono::steady_clock::now();
#endif
  for (int it = 0; it < expected_iterations(3); ++it) {
    go.jacobian_apply(x, y);
  }
#ifndef MEASURE_LOP
  auto stop = std::chrono::steady_clock::now();
  std::cout << "TIME " << std::chrono::duration<double>(stop - start).count() << std::endl;
#else
  std::cout << "TIME " << lop.dump_timers() << std::endl;
#endif
}
