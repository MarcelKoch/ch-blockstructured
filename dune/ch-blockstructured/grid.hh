#pragma once


#ifdef STRUCTURED
#include <dune/grid/yaspgrid.hh>
using Grid = Dune::YaspGrid<2>;
void print_grid_type() { std::cout << "GM affine" << std::endl; }
#else
#include <dune/grid/uggrid.hh>
using Grid = Dune::UGGrid<2>;
void print_grid_type() { std::cout << "GM multilinear" << std::endl; }
#endif
#include <dune/grid/utility/structuredgridfactory.hh>

using GV = typename Grid::LeafGridView;


