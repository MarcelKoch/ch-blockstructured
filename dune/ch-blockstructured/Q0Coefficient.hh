#pragma once

#include <dune/pdelab/gridfunctionspace/blockstructured/localfunctionspace.hh>
#include <dune/pdelab/gridfunctionspace/blockstructured/lfsindexcache.hh>
#include <dune/pdelab/backend/common/uncachedvectorview.hh>
#include <dune/pdelab/constraints/noconstraints.hh>
#include <dune/pdelab/gridfunctionspace/localvector.hh>


template <typename GFS, typename X>
class Q0Coefficient {
  using GV = typename GFS::Traits::GridViewType;
  using Entity = typename GV::template Codim<0>::Entity;

public:
  Q0Coefficient(const GFS& gfs, const X& x)
      : lfs(gfs), lfsc(lfs, Dune::PDELab::EmptyTransformation{}, false), x_view(x), xl(gfs.maxLocalSize()) {}

  void bind(const Entity& e){
    lfs.bind(e);
    lfsc.update();
    x_view.bind(lfsc);
    x_view.read(xl);
    x_view.unbind();
  }

  decltype(auto) eval(const std::size_t i) const{
    return xl(lfs, i);
  }

private:
  using LFS = Dune::PDELab::Blockstructured::LocalFunctionSpace<GFS>;
  using LFSC = Dune::PDELab::Blockstructured::LFSIndexCache<LFS, Dune::PDELab::EmptyTransformation>;

  LFS lfs;
  LFSC lfsc;

  using X_VIEW = typename X::template ConstLocalView<LFSC>;
  using X_LOCAL = Dune::PDELab::LocalVector<double>;

  X_VIEW x_view;
  X_LOCAL xl;
};
