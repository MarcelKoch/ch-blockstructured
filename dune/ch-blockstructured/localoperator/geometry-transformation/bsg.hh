#pragma once

#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/pdelab/common/quadraturerules.hh"
#include "dune/pdelab/finiteelement/localbasiscache.hh"
#include "dune/localfunctions/lagrange/lagrangecube.hh"
#include "dune/common/reservedvector.hh"
#include "dune/common/diagonalmatrix.hh"
#include "../../fill_quadrature_cache.hh"
#include "../defines.hh"


class LOP: public Dune::PDELab::LocalOperatorDefaultFlags,
           public Dune::PDELab::FullVolumePattern{
public:
  static constexpr bool doAlphaVolume = true;
  static constexpr bool doPatternVolume = true;

  static constexpr std::size_t k = N_BLOCKS;
  static constexpr std::size_t n_q = quad_size(QUAD_ORDER, 2);
  static constexpr std::size_t n_b = 4;

  template<typename GV>
  explicit LOP(const GV& gv)  {
    const auto e = *gv.template begin<0>();
    const auto geo = e.geometry();
    fillQuadraturePointsCache(geo, QUAD_ORDER, qp);
    fillQuadratureWeightsCache(geo, QUAD_ORDER, qw);
  }

  void reset_timers() { duration = 0.; }

  auto dump_timers(){
    return duration;
  }

  template<typename R, typename X, typename EG, typename LFSU, typename LFSV>
  void alpha_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, R &r) const { }

  template<typename EG, typename LFSU, typename X, typename LFSV, typename Y>
  void jacobian_apply_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, Y &y) const {
    const auto geo = eg.entity().geometry();

    auto t_start = std::chrono::steady_clock::now();
    for (std::size_t subel_y = 0; subel_y < k; ++subel_y)
      for (std::size_t subel_x = 0; subel_x < k; ++subel_x) {
#ifdef STRUCTURED
        Dune::FieldVector<double, 2> p = {subel_x * 1. / k, subel_y * 1. / k};
        const auto jit = geo.jacobianInverseTransposed(p);
        const auto detjac = geo.integrationElement(p);
        const double factor = detjac / double(k * k);
#endif

        double x_l[4] = {}, y_l[4] = {0, 0, 0, 0};
        for (std::size_t iy = 0; iy < 2; ++iy)
          for (std::size_t ix = 0; ix < 2; ++ix)
            x_l [ix + 2 * iy] = x(lfsv, (subel_y + iy) * (k + 1) + subel_x + ix);

        for (std::size_t q = 0; q < n_q; ++q) {
          std::array<JacobianType, n_b> grad;
          const auto &js_Q1 = cache.evaluateJacobian(qp[q], basis);

#ifndef STRUCTURED
          Dune::FieldVector<double, 2> p = {(qp[q][0] + subel_x) / k,
                                            (qp[q][1] + subel_y) / k};
          const auto jit = geo.jacobianInverseTransposed(p);
          const auto detjac = geo.integrationElement(p);
          const double factor = detjac / double(k * k);
#endif
          // compute gradients of basis functions in transformed element
          for (std::size_t i = 0; i < n_b; ++i) {
            jit.usmv(k, js_Q1[i][0], grad[i][0]);
          }

          double gradu[2] = {};
          for (std::size_t idim0 = 0; idim0 <= 1; ++idim0) {
            for (std::size_t iy = 0; iy < 2; ++iy)
              for (std::size_t ix = 0; ix < 2; ++ix)
                gradu[idim0] += x_l[ix + 2 * iy] * grad[ix + iy * 2][0][idim0];
          }

          const double factor_q = factor * qw[q];
          for (std::size_t iy = 0; iy < 2; ++iy)
            for (std::size_t ix = 0; ix < 2; ++ix)
              y_l[ix + 2 * iy] +=  (grad[ix + iy * 2][0][0] * gradu[0] +
                                    grad[ix + iy * 2][0][1] * gradu[1]) * factor_q;
        }
        for (std::size_t iy = 0; iy < 2; ++iy)
          for (std::size_t ix = 0; ix < 2; ++ix)
            y.accumulate(lfsv, (subel_y + iy) * (k + 1) + subel_x + ix, y_l[ix + 2 * iy]);
      }
    duration += std::chrono::duration<double>(std::chrono::steady_clock::now() - t_start).count();
  }

private:
  using Basis = Dune::Impl::LagrangeCubeLocalBasis<double, double, 2, 1>;
  Basis basis;

  using JacobianType = typename Basis::Traits::JacobianType;

  Dune::PDELab::LocalBasisCache<Basis> cache;

  Dune::ReservedVector<Dune::FieldVector<double, 2>, n_q> qp;
  Dune::ReservedVector<double, n_q> qw;

  mutable double duration = 0.;
};
