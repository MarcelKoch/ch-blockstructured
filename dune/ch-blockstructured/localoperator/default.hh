#pragma once

#include <dune/pdelab/localoperator/flags.hh>
#include <dune/pdelab/localoperator/pattern.hh>
#include <dune/pdelab/common/quadraturerules.hh>
#include <dune/pdelab/finiteelement/localbasiscache.hh>


class Q1_LOP: public Dune::PDELab::LocalOperatorDefaultFlags,
    public Dune::PDELab::FullVolumePattern{
  using Basis = Dune::Impl::LagrangeCubeLocalBasis<double, double, 2, 1>;
  Basis basis;

  using JacobianType = typename Basis::Traits::JacobianType;

  Dune::PDELab::LocalBasisCache<Basis> cache;
public:
  static constexpr bool doAlphaVolume = true;
  static constexpr bool doPatternVolume = true;

  template<typename R, typename X, typename EG, typename LFSU, typename LFSV>
  void alpha_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, R &r) const { }

  template<typename EG, typename LFSU, typename J, typename X, typename LFSV>
  void jacobian_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, J& jac) const
  {
    auto cell_geo = eg.entity().geometry();
    const auto quadrature_rule = Dune::quadratureRule(cell_geo, 2);
    auto lfsv_size = lfsv.size();
    const auto jit = cell_geo.jacobianInverseTransposed({});
    const auto detjac = cell_geo.integrationElement({});

    for (const auto &qp: quadrature_rule) {
      const auto &js_Q1 = cache.evaluateJacobian(qp.position(), basis);
      std::array<JacobianType, 4> grad = {};
      for (int i = 0; i < 4; ++i) {
        jit.umv(js_Q1[i][0], grad[i][0]);
      }
      const auto factor = detjac * qp.weight();
      for (int jy = 0; jy < 2; ++jy)
        for (int jx = 0; jx < 2; ++jx)
          for (int iy = 0; iy < 2; ++iy)
            for (int ix = 0; ix < 2; ++ix)
              jac.accumulate(lfsv, iy + ix, lfsu, jy + jx,
                             (grad[ix + iy * 2][0][0] * grad[jx + jy * 2][0][0] +
                              grad[ix + iy * 2][0][1] * grad[jx + jy * 2][0][1]) * factor);
    }
  }

  template<typename EG, typename LFSU, typename X, typename LFSV, typename Y>
  void jacobian_apply_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, Y &y) const {}
};
