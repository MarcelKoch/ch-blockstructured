#pragma once

#define MCA_START(NAME) __asm volatile("# LLVM-MCA-BEGIN " #NAME);
#define MCA_STOP(NAME) __asm volatile("# LLVM-MCA-END " #NAME);

#define __SSC_MARK(x) do{ __asm__ __volatile__("movl %0, %%ebx; .byte 100, 103, 144" : :"i"(x) : "%ebx"); } while(0)

#ifndef QUAD_ORDER
#define QUAD_ORDER 2
#endif

constexpr std::size_t quad_size(const std::size_t quadOrder, const std::size_t dim){
  std::size_t _1d = (quadOrder + 2) / 2;  // fast ceil
  std::size_t result = _1d;
  for (int i = 0; i < dim - 1; ++i)
    result *= _1d;
  return result;
}
