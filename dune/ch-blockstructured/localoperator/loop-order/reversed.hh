#pragma once

#define REVERSE 1

#ifdef NOSIMD
#include "dune/ch-blockstructured/localoperator/vectorization/default.hh"
#else
#if PERMUTE
#include "dune/ch-blockstructured/localoperator/vectorization/vec-permute.hh"
#else
#include "dune/ch-blockstructured/localoperator/vectorization/vec-simple.hh"
#endif
#endif
