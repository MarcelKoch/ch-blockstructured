#pragma once

#include <dune/vectorclass/upstream/vectorclass.h>

#ifdef SIMD8
using SIMD = Vec8d;
constexpr int stride = 8;
SIMD permute(const SIMD& v){
  return permute8d<-1,0,1,2,3,4,5,6>(v);
}
SIMD carry(const SIMD& v){
  return permute8d<7,-1,-1,-1,-1,-1,-1,-1>(v);
}
#else
#ifdef SIMD4
using SIMD = Vec4d;
constexpr int stride = 4;
SIMD permute(const SIMD& v){
  return permute4d<-1,0,1,2>(v);
}
SIMD carry(const SIMD& v){
  return permute4d<3,-1,-1,-1>(v);
}
#else
#ifdef SIMD2
using SIMD = Vec2d;
constexpr int stride = 2;
SIMD permute(const SIMD& v){
  return permute2d<-1,0>(v);
}
SIMD carry(const SIMD& v){
  return permute2d<1,-1>(v);
}
#else
class SIMD{
public:
  SIMD() = default;
  SIMD(double d_) : d(d_) {}

  SIMD(const SIMD&) = default;
  SIMD(SIMD&&) = default;

  SIMD& operator=(const SIMD&) = default;
  SIMD& operator=(SIMD&&) = default;

  SIMD& operator+=(const double v) {
    d += v;
    return *this;
  }

  operator double() const { return d;}

  double operator[](int) const {
    return d;
  }
  SIMD & load(double const * p){
    d = *p;
    return *this;
  }
  void store(double * p) const {
    *p = d;
  }
private:
  double d;
};
constexpr int stride = 1;
SIMD permute(const SIMD& v){
  return v;
}
SIMD carry(const SIMD& v){
  return v;
}
#endif
#endif
#endif
